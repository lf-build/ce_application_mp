﻿using System.Collections.Generic;

namespace CreditExchange.Application
{
    public class OfferUpdated
    {
        public IList<ILoanOffer> NewLoanOffer { get; set; }

        public IList<ILoanOffer> OldLoanOffer { get; set; }
        public string ApplicationNumber { get; set; }
    }
}
