﻿

using CreditExchange.Applicant;

namespace CreditExchange.Application
{
    public class ApplicationAddressModified
    {
        public IAddress Address { get; set; }
        public string ApplicationNumber { get; set; }
    }
}
