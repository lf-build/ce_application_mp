﻿using System.Collections.Generic;
using CreditExchange.Applicant;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;

namespace CreditExchange.Application {
    public interface IApplicationResponse {
        double RequestedAmount { get; set; }
        LoanFrequency RequestedTermType { get; set; }
        double RequestedTermValue { get; set; }
        string PurposeOfLoan { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ISource, Source>))]
        ISource Source { get; set; }
        string ApplicantId { get; set; }
        string ApplicationNumber { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IEmploymentDetail, EmploymentDetail>))]
        IEmploymentDetail EmploymentDetail { get; set; }
        string ResidenceType { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ISelfDeclareExpense, SelfDeclareExpense>))]
        ISelfDeclareExpense SelfDeclareExpense { get; set; }
        TimeBucket ApplicationDate { get; set; }
        TimeBucket ExpiryDate { get; set; }
    }
}