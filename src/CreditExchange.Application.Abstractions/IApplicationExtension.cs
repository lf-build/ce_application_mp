﻿using CreditExchange.Applicant;

namespace CreditExchange.Application
{
    public interface IApplicationExtension
    {
         IApplication Application { get; set; }

         IApplicant Applicant { get; set; }

         string StatusWorkFlowId { get; set; }
    }
}