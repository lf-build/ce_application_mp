﻿
namespace CreditExchange.Application
{
    public enum OfferFeeType 
    {
        OneTime = 1,
        Recurring = 2,
        PercentageOfLoanAmount = 3
    }
}
