﻿using System.Collections.Generic;
using CreditExchange.Applicant;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;

namespace CreditExchange.Application {
    public class ApplicationResponse : IApplicationResponse {
        public ApplicationResponse () { }

        public ApplicationResponse (IApplication application, string workFlowStatusId) {
            ApplicationNumber = application.ApplicationNumber;
            RequestedAmount = application.RequestedAmount;
            RequestedTermType = application.RequestedTermType;
            RequestedTermValue = application.RequestedTermValue;
            PurposeOfLoan = application.PurposeOfLoan;
            Source = application.Source;
            EmploymentDetail = application.EmploymentDetail;
            ResidenceType = application.ResidenceType;
            ApplicationDate = application.ApplicationDate;
            ExpiryDate = application.ExpiryDate;
            SelfDeclareExpense = application.SelfDeclareExpense;
            ApplicantId = application.ApplicantId;
            IncomeInformation = application.IncomeInformation;
            //ApplicationStatus = application.ApplicationStatus;
             WorkFlowStatusId = workFlowStatusId;
             ProductId = application.ProductId;
        }

        public double RequestedAmount { get; set; }
        public LoanFrequency RequestedTermType { get; set; }
        public double RequestedTermValue { get; set; }
        public string PurposeOfLoan { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ISource, Source>))]
        public ISource Source { get; set; }
        public string ApplicationNumber { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IEmploymentDetail, EmploymentDetail>))]
        public IEmploymentDetail EmploymentDetail { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IIncomeInformation, IncomeInformation>))]
        public IIncomeInformation IncomeInformation { get; set; }
        public string ResidenceType { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ISelfDeclareExpense, SelfDeclareExpense>))]
        public ISelfDeclareExpense SelfDeclareExpense { get; set; }
        public TimeBucket ApplicationDate { get; set; }
        public TimeBucket ExpiryDate { get; set; }
        public string ApplicantId { get; set; }
        //public string ApplicationStatus { get; set; }

        public string WorkFlowStatusId {get;set;}

        public string ProductId { get; set; }
    }
}