﻿using System;
using System.Collections.Generic;
using CreditExchange.Applicant;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Application {
    public interface IApplicationUpdateRequest {
        double RequestedAmount { get; set; }
        LoanFrequency RequestedTermType { get; set; }
        double RequestedTermValue { get; set; }
        string PurposeOfLoan { get; set; }
        Source Source { get; set; }
        string ApplicationNumber { get; set; }
        LoanOffer InitialOffer { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ILoanOffer, LoanOffer>))]
        List<ILoanOffer> GivenOffers { get; set; }
        LoanOffer SelectedOffer { get; set; }
        double MonthlyRent { get; set; }
        EmploymentDetail EmploymentDetail { get; set; }
        string ResidenceType { get; set; }
        SelfDeclareExpense SelfDeclareExpense { get; set; }
        double Emi { get; set; }
        double MonthlyExpenses { get; set; }
        double CreditCardBalances { get; set; }
        DateTimeOffset ApplicationDate { get; set; }
        string CurrentAddressYear { get; set; }
        string CurrentAddressMonth { get; set; }
        string EmployementYear { get; set; }
        string EmployementMonth { get; set; }
    }
}