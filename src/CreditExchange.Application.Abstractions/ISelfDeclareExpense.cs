﻿namespace CreditExchange.Application
{
    public interface ISelfDeclareExpense
    {
        double CreditCardBalances { get; set; }
        double DebtPayments { get; set; }
        double MonthlyExpenses { get; set; }
        double MonthlyRent { get; set; }
    }
}