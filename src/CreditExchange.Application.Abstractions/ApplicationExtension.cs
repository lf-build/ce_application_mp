﻿﻿using CreditExchange.Applicant;

namespace CreditExchange.Application
{
    public class ApplicationExtension : IApplicationExtension
    {
        public IApplication Application { get; set; }

        public IApplicant Applicant { get; set; }

        public string StatusWorkFlowId { get; set; }
    }
}